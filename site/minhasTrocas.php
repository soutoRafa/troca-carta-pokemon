<?php
	/* Codigo de proposta
	 	1 - em andamento
	 	2 - aceita
	 	3 - Recusada
	 	4 - confirmada
	 	5 - cancelada
	*/
	require_once "../includes/header.php";
	require_once 'verificaLogin.php';
	require_once '../classes/classCards.php';
	$card = new cards();
	$stts = 1;
	$result = $card->cartaDesejada($id,$stts);
	$oferta = $card->cartaOfertada($id,$stts);
	$reg = $card->propostaFeita($id);
	$offeita = $card->propostaOferecida($id);

?>
<section id="propostas-recebidas">
<div class="row">
<div class="container">
	<div class="col-md-6">
	<h3>Propostas Recebidas</h3>	
		<div class="table-responsive">
  			<form action="respondeTroca.php" method="post">
  			<table class="table">
    			<thead>    			 
				    <tr>
				      <th>#</th>
				      <th>Carta Oferecida</th>
				      <th>De</th>
				      <th>Sua Carta</th>				     
				      <th></th>
				      <th></th>
				    </tr>
				</thead>
				<tbody>
				<?php 
				$linha2 = pg_fetch_all($oferta);
				$num_linhas = pg_num_rows($oferta);
				
				if ($num_linhas > 0) {
						
				foreach ($linha2 as $l2) {
					$arr2[] = array('0' => $l2['proposta'],'1' => $l2['carta2'], '2' => $l2['nome']);
				}
					
				foreach($arr2 as $a2){
					$linha = pg_fetch_object($result);

					$a2[3] = $linha->carta;
			
				?>
					
			      <tr>
			        <td><?php echo $a2[0]; ?></td>
			        <td><?php echo $a2[1]; ?></td>
			        <td><?php echo $a2[2]; ?></td>
			        <td><?php echo $a2[3]; ?></td>
			    	<input type="hidden" name="status" value="<?php echo $a2[0];  ?>">			       
			        <td><button class="btn btn-success" name="aceitar">Aceitar</button></td>
			        <td><button class="btn btn-danger" name="recusar">Recusar</button></td>
			      </tr>
			    <?php }

				} ?>
			   
				</tbody>
			</form>			
    		</table>
		</div>
	</div>
	<div class="col-md-6">
	<h3>Propostas Feitas</h3>	
		<div class="table-responsive">
		<form action="respondeTroca.php" method="post">
  			<table class="table">
    			<thead>
				    <tr>
				    <th>#</th>
				      <th>Para</th>
				      <th>Carta</th>
				      <th>Carta Oferecida</th>
				      <th> </th>
				    </tr>
				</thead>
				<tbody>
				<?php
				$feita = pg_fetch_all($reg);
				$num_linhas2 = pg_num_rows($reg);

				if ($num_linhas2 > 0) {
					
				foreach($feita as $feita){
					$arr[] = array('1' => $feita['nome'], '2' => $feita['carta']);
					
				}
				foreach($arr as $a){
					$feita2 = pg_fetch_object($offeita);
					$a[0] = $feita2->proposta;
					$a[3] = $feita2->carta;
			
				?>
			      <tr>
			        <td><?php echo $a[0]; ?></td>
			        <td><?php echo $a[1]; ?></td>
			        <td><?php echo $a[2]; ?></td>
			        <td><?php echo $a[3]; ?></td>
			        <input type="hidden" name="status" value="<?php echo $feita2->proposta;  ?>">
			        <td><button class="btn btn-danger" name="cancelar">Cancelar</button></td>
			      </tr>
			    <?php }
			   		}  
			   	?>
				
				</tbody>
    		</table>
    	</form>	
		</div>
	</div>
</form>	 	 	
</div>
</div>
</section>
<?php
	require_once "../includes/footer.php";
?>	