<?php
	
	$login = $_SESSION['login'];

	require_once '../classes/classLogin.php';
	require_once '../classes/crud.php';
	require_once '../classes/classCards.php';
	$crud = new crud();
	
	$reg = $crud->id($login);
	$id = $reg->codigo;
	
	$user = $crud->selectUsuario($id);
	$end  = $crud->selectEndereco($id);

	$card = new cards();
 	$linhas = $card->verificaTroca($id);

 	$stts = 2;
 	$carta1 = $card->cartaDesejada($id,$stts);
	$carta2 = $card->cartaOfertada($id,$stts);