<?php 
    require_once 'verificaLogin.php';
    require_once '../classes/classCards.php';
    $confirma = 4;
    $cancela = 3;
    $proposta = $_POST['proposta'];
    $propondo = $_POST['propondo'];
    $cartaDesejada = $_POST['carta1'];
    $cartaProposta = $_POST['carta2'];
    
    $card = new cards();

    if(isset($_POST['confirma'])){
    	$card->mudaStatus($confirma, $proposta);

    	$u1 = $card->selectCarta($id, $cartaDesejada);
    	$u2 = $card->selectCarta($propondo, $cartaProposta);
        
    	while($linha = pg_fetch_object($u1)){
			$user1 = $linha->cd_usuario;
			$card1 = $linha->cd_carta;
			$qtd1 = $linha->qtd;

    	}
        //var_dump($cartaProposta);
    	while($linha2 = pg_fetch_object($u2)){
	    	$user2 = $linha2->cd_usuario;
			$card2 = $linha2->cd_carta;
			$qtd2  = $linha2->qtd;
    	}

    	$card->deletaCarta($user1, $card1);
    	$card->deletaCarta($user2, $card2);
    	
        // Teste se a pessoa tem mais de uma carta que ta sendo trocada
        // Se ela tiver mais de uma, ela diminui a quantidade e reinsere

    	if($qtd1 > 1){	
    		$qtd1 = $qtd1 - 1;
    		$card->insertCarta($user1, $card1, $qtd1);
    	}
    	if ($qtd2 > 1) {
    		$qtd2 = $qtd2 - 1;
    		$card->insertCarta($user2, $card2, $qtd2);
    	}
        //user1 = Rafael user2 = Diego card1 = charmander card2 = Totodile
        $reg1 = $card->verificaQuantidade($user1, $card2);
        $reg2 = $card->verificaQuantidade($user2, $card1);
        $q1 = $reg1->qtd;
        $q2 = $reg2->qtd;
        
        //Se o usuario já possuir a carta ofertada
        if($q1 > 0){
            $total1 = $q1 + 1;
            $card->atualizaQuantidade($user1, $card2, $total1);    
        }else{
            $um = 1;
            $card->insertCarta($user1, $card2, $um);
        }

        if($q2 > 0){
            $total2 = $q2 + 1;
            $card->atualizaQuantidade($user2, $card1, $total2);    
        }else{	
        	$um = 1;
        	$card->insertCarta($user2, $card1, $um);
    	}
    }
    if (isset($_POST['descarta'])) {
    	$card->mudaStatus($cancela, $proposta);
    }

  header('location: user.php');