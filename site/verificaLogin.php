<?php
	session_start();
	
	$login = $_SESSION['login'];
	$_SESSION['mensagem'] = "Seja bem vindo, ".$login;

	if ($_SESSION['logado'] == false) {
		header('location: ../login.php');
	}

	require_once '../classes/crud.php';

	$crud = new crud();
	$reg = $crud->id($login);
	$id = $reg->codigo;
	$_SESSION['id'] = $id;
