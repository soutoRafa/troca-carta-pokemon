<?php 
	require_once "../includes/header.php";
	require_once 'verificaLogin.php';
	require_once 'pickCard.php';

?>
<section id="cartas">
	<div class="col-md-12">
		<div class="row">
			<div class="page-header"><h1>Pesquise uma carta</h1></div>
			<input type="text" name="pesquisa" id="pesquisa" placeholder="Pesquise uma carta...">
			<button type="submit" id="search"><i class="fa fa-search fa-2x"></i></button>
			
		</div>
	</div>
	<div class="container">
		<?php
			while($linha = pg_fetch_object($psq)){
		?>
		<div class="card-container">
 			<form action="troca.php" method="post">
 				
 				<img src="cartas/<?php echo $linha->img; ?>">
 				
 				<label><?php echo $linha->nome; ?></label><Br>
 				<p>De: 	<?php echo $linha->user; ?>	</p>
				<p>Quantidade: <?php echo $linha->qtd; ?></p> 				
 				
 				<input type="hidden" name="dono" value="<?php echo $linha->cd_user; ?>">
 				<button type="submit" name="adicionar" class="btn btn-success" value="<?php echo $linha->codigo;?>">Trocar</button>
 			
 			</form>	
 		</div>
 		<?php } ?>
	</div>
</section>
<?php require_once '../includes/footer.php';?>