<?php
	
	$nome  = $_POST['nome'];
	$login = $_POST['login'];
	$senha = $_POST['senha'];
	$email = $_POST['email'];
	$novasenha = $_POST['novasenha'];
	$id = $_POST['id'];

	require_once '../classes/crud.php';
	require_once '../classes/usuario.php';

	$crud = new crud();
	$usuario = new usuario();
	
	
	if ($senha != $novasenha) {
		header('location: perfil.php');
	}
	if (!isset ($_POST)) {
		header('location: perfil.php');
	}

	$usuario->setNome($nome);
	$usuario->setLogin($login);
	$usuario->setSenha($senha);
	$usuario->setEmail($email);
	
	$crud->updateUsuario($usuario, $id);
	header('location: perfil.php');
	