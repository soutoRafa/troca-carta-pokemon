<?php 
	require_once "../includes/header.php";
	require_once 'verificaLogin.php';
	require_once 'pickCard.php';
?>
<section id="cartas">
	<div class="page-header"><h1>Marque seus cards!</h1></div>
	<div class="container">
		<?php
			while($linha = pg_fetch_object($reg)){
		?>
		<div class="card-container">
 			<form action="adicionaCarta.php" method="post">
 				
 				<img src="cartas/<?php echo $linha->img; ?>">
 				
 				<label><?php echo $linha->nome; ?></label><Br>
 				
 				<label>Quantidade</label>
 				<input type="number" name="quantidade" min="1">
 				
 				<button type="submit" name="adicionar" class="btn btn-success" value="<?php echo $linha->codigo;?>">Adicionar</button>
 			
 			</form>	
 		</div>
 		<?php } ?>
	</div>
</section>
<?php require_once '../includes/footer.php';?>