<?php

	require_once "../includes/header.php";
	require_once 'verificaLogin.php';
	require_once 'pickCard.php';
	$carta = $_POST['adicionar'];
	$dono = $_POST['dono']; 
	$proposta = $card->propostaCarta($carta, $dono);

?>
<section id="proposta">
	<div class="page-header"><h1>Faça sua proposta!</h1></div>
		<form action="efetuaTroca.php" method="post">
		<div class="col-md-12 propondo">
		<?php if($rowCard == 0){?>
			<div class="container">		
				<div class="alert alert-danger erro">
					<h3>Você não possui cartas</h3>
					<p><a href="">Clique aqui</a> para adicionar suas cartas e começar a trocar!</p>
				</div>
			</div>	
		<?php }else{ ?>
			<h2>Proponha suas cartas</h2>
			<div class="container">	
				<div class="row">
				<?php
					while($linha = pg_fetch_object($result)){
				?>
				<div class="card-container">
					<img src="cartas/<?php echo $linha->img; ?>">
 				
 					<label><?php echo $linha->nome; ?></label><Br>
 				
 					
 					<div class="radio">
 						<input type="radio" name="propondo" value="<?php echo $linha->cd_carta; ?>">
 					</div>				
				</div>
				<?php } ?>
				</div>
			</div>
		</div>
		<div class="col-md-12 propondo">
			<h2>Cartas do proposto</h2>
			<div class="container">
					<div class="row">
						<?php
							while($linha = pg_fetch_object($proposta)){
						?>
						<div class="card-container">
							<img src="cartas/<?php echo $linha->img; ?>">
 							<label><?php echo $linha->nome; ?></label><br>
 							<label><?php echo $linha->dono; ?></label><br>
 			 				<input type="hidden" name="dono" value="<?php echo $linha->cod_dono; ?>">
				
 							<input type="hidden" name="carta" value="<?php echo $linha->codigo; ?>">
						</div>
						<?php } ?>				
				</div>	
			</div>
		</div>
		
			<button type="submit" class="btn btn-success btn-lg">Enviar proposta!</button>
		<?php } ?>
		</form>
</section>
<?php require_once '../includes/footer.php';
