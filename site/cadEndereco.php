<?php
	session_start();
	$login = $_SESSION['login'];

	$rua = $_POST['rua'];
	$num = $_POST['numero'];
	$bairro = $_POST['bairro'];
	$tel = $_POST['telefone'];
	$cidade = $_POST['cidade'];
	$uf = $_POST['uf'];

	require_once '../classes/conecta.php';
	require_once '../classes/crud.php';
	require_once '../classes/usuario.php';

	$conn = new conexao();
	$usuario = new usuario();
	$crud = new crud();	

	$usuario->setRua($rua);
	$usuario->setNumero($num);
	$usuario->setBairro($bairro);
	$usuario->setTelefone($tel);
	$usuario->setCidade($cidade);
	$usuario->setUf($uf);
	
	$reg = $crud->id($login);
	$id = $reg->codigo;
	$crud->insertEndereco($usuario, $id);

	if (isset($_POST['exclui'])) {
		$crud->deleteEnd($id);
	}

	header('location: perfil.php');