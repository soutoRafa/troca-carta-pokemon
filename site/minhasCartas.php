<?php 
	require_once "../includes/header.php";
	require_once 'verificaLogin.php';
	require_once 'pickCard.php';

?>
<section id="cartas">
	<div class="page-header"><h1> Suas Cartas </h1></div>

	<div class="container">
		<?php if($rowCard == 0){?>
		<div class="alert alert-danger erro">
			<h3>Você não possui cartas</h3>
			<p><a href="cartas.php">Clique aqui</a> para adicionar suas cartas!</p>
		</div>
		<?php } ?>
		<?php
			while($linha = pg_fetch_object($result)){
		?>
			
			<div class="card-container">
			<form action="retira.php" method="post">
				
				<img src="cartas/<?php echo $linha->img; ?>">
 				<label><?php echo $linha->nome; ?></label><Br> 				
 				<input type="hidden" name="carta" value="<?php echo $linha->cd_carta; ?>"> 
 				
 				<p>Quantidade: <?php echo $linha->qtd; ?></p>		
 				
 				<button type="submit" name="remove"><a>Remover</a></button>
 			</form>		
			</div>
		<?php } ?>

	</div>	
</section>
<?php require_once '../includes/footer.php';?>