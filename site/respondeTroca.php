<?php
	require_once 'verificaLogin.php';
	require_once '../classes/classCards.php';
	$card = new cards();
	$proposta = $_POST['status'];
	$aceita = 2;
	$recusada = 3;
	$cancelada = 5;

	if(isset($_POST['aceitar'])){
		$card->mudaStatus($aceita, $proposta);
	}
	if(isset($_POST['recusar'])){
		$card->mudaStatus($recusada, $proposta);		
	}
	if(isset($_POST['cancelar'])){
		$card->mudaStatus($cancelada, $proposta);
	}
	
	header('location: user.php');	