<?php 
    require_once 'verificaLogin.php';
    require_once 'perfilUsuario.php';
    require_once "../includes/header.php"; 

?>
    <section class="corpo">
    <section id="conteudo-inicial">
        <div class="container"> 
           <img src="../imagens/userball.png" id="usuario-bola">
            <h3 id="bem-vindo"><?php echo $_SESSION['mensagem']; ?></h3>
            <div class="row">
                <div class="box-conteudo">
                    <a href="perfil.php"><i class="fa fa-user-circle fa-5x" aria-hidden="true"></i>
                    <h3> Editar perfil </h3></a>
                </div>
                <div class="box-conteudo">
                    <a href="cartas.php"><i class="fa fa-suitcase fa-5x" aria-hidden="true"></i>
                    <h3>Adicione suas cartas</h3></a>
                </div>
                <div class="box-conteudo">
                    <a href="pesquisaCarta.php"><i class="fa fa-search fa-5x" aria-hidden="true"></i>
                    <h3>Procure a carta que você precisa!</h3></a>
                </div>
            </div>

            <?php if($linhas > 0){?>
            <div class="alert alert-success">
                <h3>Essa troca foi finalizada?</h3>
                <table class="table">
                <thead>
                    <th>Com o usuário</th>
                    <th>Sua Carta</th>
                    <th>Por</th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                <form action="confirmaTroca.php" method="post">
                <?php while ($linha = pg_fetch_object($carta1)) { ?>
                    <?php while ($linha2 = pg_fetch_object($carta2)) { ?>
                        <td><?php echo $linha2->nome; ?></td>
                        <td><?php echo $linha->carta; ?></td>
                        <td><?php echo $linha2->carta2; ?> </td>
                         
                        <input type="hidden" name="proposta" value="<?php echo $linha2->proposta?>">
                        <input type="hidden" name="propondo" value="<?php echo $linha->cd_propondo; ?>">
                        <input type="hidden" name="carta1" value="<?php echo $linha->carddesejo; ?>">
                        <input type="hidden" name="carta2" value="<?php echo $linha2->cardoferta; ?>">
                        <td><button class="btn btn-primary" name="confirma">Confirmar</button></td>
                        <td><button class="btn btn-danger" name="descarta">Descartar</button></td>                      
                    <?php } ?>
                <?php } ?>

                </form>
                </tbody>
                </table>
            </div>
           <?php } ?>    
        </div>
    </section>    
    </section>
    <?php require_once "../includes/footer.php" ?>
