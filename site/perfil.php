<?php
	require_once 'verificaLogin.php'; 
	require_once 'perfilUsuario.php';
	require_once "../includes/header.php";  
?>

<section class="corpo">
	<section id="editar-perfil">
		<div class="page-header">
			<h1> Editar perfil </h1>
		</div>
		<div class="col-md-6">
			<span>Dados pessoais</span>
			<?php  /*echo '<pre>'; var_dump($user); die;*/  ?>
				<form method="post" class="form-update" action="updateUsuario.php">
					<input type="text" name="nome" placeholder="Nome" value="<?php echo $user->nome;?>">
					<input type="text" readonly="readonly" name="login" placeholder="Login" value="<?php echo $user->login; ?>">
					
					<input type="password" name="senha" placeholder="Senha"  value="<?php echo $user->senha; ?>">
					<input type="password" name="novasenha" placeholder="Nova senha" value="<?php echo $user->senha; ?>">
					<input type="email" name="email"  placeholder="Email"  value="<?php echo $user->email; ?>">
					<input type="hidden" name="id" value="<?php echo $id; ?>">
					<button type="submit" class="edit" name="att_dados">Enviar</button>
				</form>
			
		</div>
		<div class="col-md-6">
			<span>Endereço</span>
				<form method="post" action="cadEndereco.php" class="form-update border-none">
					<button type="submit" class="btn btn-link" name="exclui">excluir endereço </button>
					<input type="text" name="uf" placeholder="Sigla do estado" value="<?php echo (isset($end->uf)?$end->uf:"") ?>">
					<input type="text" name="cidade"  placeholder="Cidade" value="<?php echo (isset($end->cidade)?$end->cidade:"") ?>">
					<input type="text" name="rua" placeholder="Rua" value="<?php echo (isset($end->rua)?$end->rua:"") ?>">
					<input type="text" name="numero" placeholder="numero" value="<?php echo (isset($end->numero)?$end->numero:"") ?>">
					<input type="text" name="bairro" placeholder="bairro" value="<?php echo (isset($end->bairro)?$end->bairro:"") ?>">
					<input type="text" name="telefone"  placeholder="telefone" value="<?php echo (isset($end->telefone)?$end->telefone:"") ?>">
					<button type="submit"  class="edit" value="add_endereco">Enviar</button>
					
				</form>
				
		</div>					
	</section>
</section>
<?php require_once "../includes/footer.php";  ?>
