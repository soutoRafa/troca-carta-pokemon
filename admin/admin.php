<?php 
	require_once 'verificaLoginAdm.php';
	require_once 'classes/classCards.php';
	
	$card = new cards();

	$reg = $card->selectTipo();
	$regraridade = $card->selectRaridade();
	$regcolec = $card->selectColecao();
	$estagio = $card->selectEstagio();
	$pkmnull = $card->selectPokemon();
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="admin.css">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/js.js"></script>
	<title>Administração</title>
</head>
<body>
<section id="corpo">
	<header>
		<nav id="menu">
			<ul>
				<li><a id="carta">Cartas</a></li>
				<li><a id="pkmns">Pokemon</a></li>
				<li><a id="moves">Moves</a></li>
				<li><a href="logOut.php">Sair</a></li>
			</ul>
		</nav>
	</header>
	<section id="cadastro-carta">
		<div class="box-cadastro">
			<form method="post" action="cadCarta.php" enctype="multipart/form-data">
			<label><h1>Adicionar Carta</h1></label>	
			<input type="text" name="nome" placeholder="Nome da carta"><br>
			
			<label> Tipo </label><br>
			<?php 
				while  ($linha = pg_fetch_object($reg)){ 
			?>	
			
			<input type="radio" name="tipo" value="<?php echo $linha->codigo?>">
			<?php 
				echo $linha->nome; 
			}	
			?><br>

			<label>Raridade</label><br>
			<?php while  ($linha = pg_fetch_object($regraridade)) 
			{ 
			?>	
			<input type="radio" name="raridade" value="<?php echo $linha->codigo?>">
			<?php 
				echo $linha->descricao; 
			}	
			?><br>

			<label>Coleção</label><br>
			<?php while  ($linha = pg_fetch_object($regcolec)) 
			{ 
			?>	
			<input type="radio" name="colecao" value="<?php echo $linha->codigo?>">
			<?php 
				echo $linha->nome; 
			}	
			?><br>

			<input type="file" name="foto"><br>
			<label>Descrição</label><br>
			<textarea name="descricao" cols="40" rows="10"></textarea>
			<button type="submit" name="enviar" class="cad-card">Cadastrar</button>

			</form>
		</div>
	</section>
	<section id="cadastro-pokemon">
		<div class="box-cadastro">
			<form method="post" action="cadPkmn.php">
			<label><h1>Adicionar Pokemon</h1></label>
				
				<label>Quem é esse pokemon?!</label><br>
				<?php 
				while  ($pokenull = pg_fetch_object($pkmnull)){ 
				?>	
				<input type="radio" name="pokemon" value="<?php echo $pokenull->codigo?>">
				<?php echo $pokenull->nome;
				} 
				?><br>

				<input type="text" name="fraqueza" placeholder="Fraqueza"><br>
				<input type="text" name="resistencia" placeholder="Resistencia"><br>
				<label>Custo de recuo</label><br>
				<input type="number" name="recuo"><br>
				<label>HP</label><br>
				<input type="number" name="hp"><br>
				<label>Estágio</label><br>
				<?php 
				while  ($stage = pg_fetch_object($estagio)){ 
				?>	
				<input type="radio" name="estagio" value="<?php echo $stage->codigo?>">
				<?php echo $stage->nome;
				} 
				?><br>
				<label>Habilidade</label><br>
				<textarea cols="40" rows="10" name="habilidade"></textarea><br>
				<button type="submit" name="enviar" class="cad-card">Cadastrar</button>

			</form>
		</div>
	</section>
	<section id="cadastro-moves">
		<div class="box-cadastro">	
			<form method="post" action="cadMove.php">
			<label><h1>Adicionar Move</h1></label>
				<input type="text" name="nome" placeholder="Nome do golpe"><br>
				<input type="text" name="dano" placeholder="Dano"><br>
				<label>Custo de energias</label><br>
				<input type="number" name="energia"><br>
				<label>Descrição do golpe</label><br>
				<textarea cols="40" rows="10" name="descricao"></textarea><br>
				<button type="submit" name="enviar" class="cad-card">Cadastrar</button>
			</form>	
		</div>	
	</section>	
</section>
</body>
</html>