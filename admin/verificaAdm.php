<?php
	session_start();
	$login = $_POST['login'];
	$senha = $_POST['senha'];

	require_once 'classes/classadm.php';

	$log = new loginAdm();
	$rows = $log->login($login, $senha);

	if ($rows == 1)
	{	
		$_SESSION['login'] = $login;
		$_SESSION['logado'] = true;
		header('location: admin.php');
	}

?>
<script type="text/javascript">
	alert('login ou senha incorretos');
	window.location.href = 'loginAdmin.php';
</script>