$(document).ready(function(){
	$('#pkmns').click(function(){
		$('#cadastro-pokemon').css('display','block');
		$('#cadastro-carta').css('display', 'none');
		$('#cadastro-moves').css('display','none');
	});
	$('#moves').click(function(){
		$('#cadastro-moves').css('display','block');
		$('#cadastro-carta').css('display', 'none');
		$('#cadastro-pokemon').css('display','none');
	});
	$('#carta').click(function(){
		$('#cadastro-carta').css('display', 'block');
		$('#cadastro-moves').css('display','none');
		$('#cadastro-pokemon').css('display','none');
	});
});