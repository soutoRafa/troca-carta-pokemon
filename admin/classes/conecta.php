<?php 

class conexao{
	private $bd = 'Poketrade';
	private $user = 'postgres';
	private $porta = 5432;
	private $senhabd = 1234;
	private $servidor = 'localhost';
	private $con;
	private $resultado;

	public function __construct(){
	$this->con = pg_connect("host=$this->servidor port=$this->porta dbname=$this->bd user=$this->user password=$this->senhabd");

		if (!$this->con) {
			die("impossível conectar ao banco de dados!");
		}
	}

	public function consulta($query){
		$this->resultado = pg_query($this->con, $query);

		return $this->resultado;
	}

	public function linhas (){
		$result = pg_num_rows($this->resultado);
		return $result;
	}
	
	public function fetch_object(){
		$result = pg_fetch_object($this->resultado);
		return $result;		
	}
	
}