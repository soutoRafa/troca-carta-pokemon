<?php

	require_once 'conecta.php';

	class pokemon
	{
		private $cardpkmn;
		private $fraqueza;
		private $resistencia;
		private $recuo;
		private $hp;
		private $stage;
		private $hab;


		function setPkmn($pkmn){
			$this->cardpkmn = $pkmn;
		}
		function setFraqueza($fraq){
			$this->fraqueza = $fraq;
		}
		function setResistencia($rest){
			$this->resistencia = $rest;
		}
		function setRecuo($rec){
			$this->recuo = $rec;
		}
		function setHp($hitpoints){
			$this->hp = $hitpoints;
		}
		function setStage($stg){
			$this->stage = $stg;
		}
		function setHabilidade($description){
			$this->hab = $description;
		}
		function getPkmn(){
			return $this->cardpkmn;
		}
		function getFraqueza(){
			return $this->fraqueza;
		}
		function getResistencia(){
			return $this->resistencia;
		}
		function getRecuo(){
			return $this->recuo;
		}
		function getHp(){
			return $this->hp;
		}
		function getHabilidade(){
			return $this->hab;
		}
		function getStage(){
			return $this->stage;
		}

		function insertPkmn($objPkmn){
			$pkmn = $objPkmn->getPkmn();
			$resist = $objPkmn->getResistencia();
			$fraq = $objPkmn->getFraqueza();
			$rec = $objPkmn->getRecuo();
			$hp = $objPkmn->getHp();
			$hb = $objPkmn->getHabilidade();
			$stg = $objPkmn->getStage();
			$conn = new conexao();
			$sql = "INSERT INTO pokemons (cd_carta,fraqueza,resistencia,recuo,hp,habilidade,cd_estagio) VALUES ('$pkmn','$fraq','$resist','$rec','$hp','$hb','$stg')";
			$conn->consulta($sql);
		}

	}