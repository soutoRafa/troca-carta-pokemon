<?php

	require_once '../classes/conecta.php';
	
	class cards 
	{
		private $nome;
		private $tipo;
		private $colecao;
		private $raridade;
		private $img;
		private $descricao;

		private $nomegolpe;
		private $custogolpe;
		private $descricaogolpe;
		private $danogolpe;

		function setNome($name){
			$this->nome = $name;
		}
		function setTipo($type){
			$this->tipo = $type;
		}		
		function setColecao($colec){
			$this->colecao = $colec;
		}
		function setRaridade($rarid){
			$this->raridade = $rarid;
		}
		function setImagem($imgcard){
			$this->img = $imgcard;
		}
		function setDescricao($desc){
			$this->descricao = $desc;
		}
		function getNome(){
			return $this->nome;
		}
		function getTipo(){
			return $this->tipo;
		}
		function getColecao(){
			return $this->colecao;
		}
		function getRaridade(){
			return $this->raridade;
		}	
		function getImagem(){
			return $this->img;
		}	
		function getDescricao(){
			return $this->descricao;
		}										
		function insertCard($objCard){
			$nome = $objCard->getNome();
			$tipo = $objCard->getTipo();
			$raridade = $objCard->getRaridade();
			$colecao = $objCard->getColecao();
			$imagem = $objCard->getImagem();
			$descricao = $objCard->getDescricao();
			
			$conn = new conexao();
			$sql = "INSERT INTO cartas(nome, cd_tipo,cd_colecao,cd_raridade, img, descricao) VALUES ('$nome','$tipo','$colecao', '$raridade', '$imagem', '$descricao')";
			$conn->consulta($sql);
		}	
	 	function selectTipo(){
	 		$conn = new conexao();
	 		$sql = "SELECT codigo, nome FROM tipos";
	 		$reg = $conn->consulta($sql);

	 		return $reg; 
	 	}
	 	function selectRaridade(){
	 		$conn = new conexao();
	 		$sql = "SELECT codigo, descricao FROM raridades";
	 		$reg = $conn->consulta($sql);

	 		return $reg; 
	 	}
	 	function selectEstagio(){
	 		$conn = new conexao();
	 		$sql  = "SELECT codigo, nome FROM estagios";
	 		$reg  = $conn->consulta($sql);

	 		return $reg;
	 	}
		function selectColecao(){
	 		$conn = new conexao();
	 		$sql = "SELECT codigo, nome FROM colecao";
	 		$reg = $conn->consulta($sql);

	 		return $reg; 
	 	}
	 	function SelectPokemon(){
	 		$conn = new conexao();
	 		$sql = "SELECT cartas.codigo, cartas.nome FROM cartas left JOIN pokemons ON (cartas.codigo = pokemons.cd_carta) where pokemons.cd_carta is null and cd_tipo <> 4";
	 		$reg = $conn->consulta($sql);

	 		return $reg;
	 	}
	
	 	function setNomeGolpe($namemove){
			$this->nomegolpe = $namemove;
		}
		function setEnergia($custo){
			$this->custogolpe = $custo;
		}		
		function setGolpeDescricao($descrico){
			$this->descricaogolpe = $descrico;
		}
		function setDano($damage){
			$this->danogolpe = $damage;
		}
		
		function getNomeGolpe(){
			return $this->nomegolpe;
		}
		function getEnergia(){
			return $this->custogolpe;
		}
		function getGolpeDescricao(){
			return $this->descricaogolpe;
		}
		function getDano(){
			return $this->danogolpe;
		}
		function insertMove($objCard){
			$nome = $objCard->nomegolpe;
			$custo = $objCard->custogolpe;
			$dano = $objCard->danogolpe;
			$desc = $objCard->descricaogolpe;
			$conn = new conexao();

			$sql = "INSERT INTO golpes(nome, dano,custo, descricao) VALUES ('$nome','$dano','$custo', '$desc')";
			$conn->consulta($sql);
		}									
	}