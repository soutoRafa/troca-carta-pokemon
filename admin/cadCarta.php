<?php

	$nome = $_POST['nome'];
	$tipo = $_POST['tipo'];
	$raridade = $_POST['raridade'];
	$colecao = $_POST['colecao'];
	$img = $_FILES['foto']['name'];
	$descricao = $_POST['descricao'];
	$temporario = $_FILES['foto']['tmp_name'];

	move_uploaded_file($temporario, "../site/cartas/$img");
	
	require_once 'classes/classCards.php';

	$card = new cards();

	$card->setNome($nome);
	$card->setTipo($tipo);
	$card->setRaridade($raridade);	
	$card->setcolecao($colecao);
	$card->setImagem($img);
	$card->setDescricao($descricao);
	$card->insertCard($card);
	
?>

<script type="text/javascript">
	alert('Carta cadastrada com sucesso');
	window.location.href ='admin.php';
</script>