-- Gera��o de Modelo f�sico
-- Sql ANSI 2003 - brModelo.



CREATE TABLE enderecos (
cd_usuario N�mero(4),
cd_endereco Texto(1) PRIMARY KEY,
telefone Texto(1),
numero Texto(1),
endere�o Texto(1)
)

CREATE TABLE propostas (
validade Texto(1),
status Texto(1),
cd_proposta Texto(1) PRIMARY KEY
)

CREATE TABLE golpes (
cd_carta Texto(1),
custo Texto(1),
cd_move Texto(1) PRIMARY KEY,
dano Texto(1),
descricao Texto(1)
)

CREATE TABLE Rela��o_6 (
cd_pkmn Texto(1),
cd_move Texto(1),
FOREIGN KEY(cd_move) REFERENCES golpes (cd_move)
)

CREATE TABLE usuarios (
senha Texto(1),
nome Texto(1),
Sexo Texto(1),
Email Texto(1),
cd_usuario Texto(1) PRIMARY KEY,
cd_endereco Texto(1),
FOREIGN KEY(cd_endereco) REFERENCES enderecos (cd_endereco)
)

CREATE TABLE cartas (
nome Texto(1),
cd_carta Texto(1) PRIMARY KEY,
cd_raridade Texto(1),
cd_tipo Texto(1),
cd_colecao N�mero(4)/*falha: chave estrangeira*//*falha: chave estrangeira*/
)

CREATE TABLE Tipos (
cd_tipo Texto(1) PRIMARY KEY,
nome Texto(1),
descricao Texto(1)
)

CREATE TABLE pokemons (
cd_pkmn Texto(1) PRIMARY KEY,
fraqueza Texto(1),
resistencia Texto(1),
habilidade Texto(1),
recuo Texto(1),
HP Texto(1),
cd_estagio Texto(1),
cd_carta N�mero(4)/*falha: chave estrangeira*/
)

CREATE TABLE estagios (
cd_estagio Texto(1) PRIMARY KEY,
nome Texto(1)
)

CREATE TABLE colecao (
nome Texto(1),
ano Texto(1),
cd_colecao Texto(1) PRIMARY KEY
)

CREATE TABLE raridades (
descricao Texto(1),
cd_raridade Texto(1) PRIMARY KEY
)

CREATE TABLE usuario_proposta (
cd_carta Texto(1),
cd_usuario Texto(1),
cd_propusu Texto(1) PRIMARY KEY,
-- Erro: nome do campo duplicado nesta tabela!
cd_carta Texto(1),
qtd Texto(1),
-- Erro: nome do campo duplicado nesta tabela!
cd_usuario Texto(1),
cd_proposta Texto(1),
FOREIGN KEY(cd_carta) REFERENCES cartas (cd_carta),
FOREIGN KEY(cd_usuario) REFERENCES usuarios (cd_usuario)/*falha: chave estrangeira*/
)

ALTER TABLE Rela��o_6 ADD FOREIGN KEY(cd_pkmn) REFERENCES pokemons (cd_pkmn)
ALTER TABLE cartas ADD FOREIGN KEY(cd_raridade) REFERENCES raridades (cd_raridade)
ALTER TABLE pokemons ADD FOREIGN KEY(cd_estagio) REFERENCES estagios (cd_estagio)
