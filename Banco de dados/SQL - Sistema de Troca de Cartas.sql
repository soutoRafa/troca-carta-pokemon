﻿
CREATE TABLE enderecos (
cd_usuario int,
codigo serial PRIMARY KEY not null,
telefone char(9),
numero varchar(10),
rua varchar(30),
bairro varchar(20),
foreign key(cd_usuario) references usuarios(codigo)
);
CREATE TABLE propostas (
validade date,
status int,
codigo serial PRIMARY KEY not null
);

CREATE TABLE golpes (
codigo serial PRIMARY KEY not null,
custo int,
dano int,
descricao varchar(50)
);
CREATE TABLE tipos (
codigo serial not null PRIMARY KEY,
nome varchar(20),
descricao varchar(150)
);

CREATE TABLE estagios (
codigo int not null PRIMARY KEY,
nome varchar(20)
);
CREATE TABLE colecao (
codigo serial not null PRIMARY KEY,
nome varchar(20),
ano char(4)
);

CREATE TABLE raridades (
codigo serial not null PRIMARY KEY,
descricao varchar(10)
);
CREATE TABLE cartas (
codigo serial not null PRIMARY KEY,
nome varchar(20),
cd_raridade int not null,
cd_tipo int not null,
cd_colecao int not null,
FOREIGN KEY(cd_raridade) REFERENCES raridades(codigo),
FOREIGN KEY(cd_tipo) REFERENCES tipos(codigo),
FOREIGN KEY(cd_colecao) REFERENCES colecao(codigo)
);
CREATE TABLE pokemons (
codigo serial not null PRIMARY KEY,
fraqueza varchar(10),
resistencia varchar(10),
habilidade varchar(200),
recuo int,
HP int,
cd_estagio int not null,
cd_carta int not null,
FOREIGN KEY(cd_estagio) REFERENCES estagios (codigo),
FOREIGN KEY(cd_carta) REFERENCES cartas(codigo)
);
CREATE TABLE golpes_pokemons (
cd_pkmn int not null,
cd_move int not null,
FOREIGN KEY(cd_move) REFERENCES golpes (codigo),
foreign key(cd_pkmn) references pokemons (codigo)
);

CREATE TABLE usuarios (
codigo serial PRIMARY KEY,
nome varchar (50),
senha varchar(20),
Sexo char(1),
Email varchar(30),
cd_endereco int not null
);
CREATE TABLE usuario_proposta (
codigo serial not null PRIMARY KEY,
qtd int,
cd_carta int not null,
cd_usuario int not null,
cd_proposta int not null,
FOREIGN KEY(cd_carta) REFERENCES cartas(codigo),
FOREIGN KEY(cd_usuario) REFERENCES usuarios(codigo),
FOREIGN KEY(cd_usuario) REFERENCES propostas(codigo)
);