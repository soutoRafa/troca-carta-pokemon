<?php
	
	require_once 'conecta.php';

	class cards
	{
		public function selectCartas()
		{
			$conn = new conexao();
			$sql = "SELECT nome, img, c.codigo 
			FROM cartas c 
			LEFT JOIN pokemons p 
			ON (c.codigo = p.cd_carta) 
			where img is not null";
			$reg = $conn->consulta($sql);
			return $reg;
		}
		public function contaCartas($id)
		{
			$conn = new conexao();
			$sql = "SELECT c.codigo
			FROM cartas c
			JOIN usuarios_cartas cu
			ON (c.codigo = cu.cd_carta)
			JOIN usuarios u
			ON (u.codigo = cu.cd_usuario)
			WHERE u.codigo = '$id'";
			$conn->consulta($sql);
			$linhas = $conn->linhas();
			return $linhas;
		}
		public function adicionaCarta($id, $card, $quantidade)
		{
			$conn = new conexao();
			$sql = "INSERT INTO usuarios_cartas(cd_usuario, cd_carta, qtd) VALUES('$id', '$card', '$quantidade')";
			$conn->consulta($sql);
		}
		public function userCards($id){
			$conn = new conexao();
			$sql = "SELECT c.nome, c.img, c.codigo as cd_carta, u.codigo, c.descricao, uc.qtd 
			FROM cartas c 
			JOIN usuarios_cartas uc 
			ON (c.codigo = uc.cd_carta) 
			JOIN usuarios u 
			ON (u.codigo = uc.cd_usuario) 
			where u.codigo = $id";
			$reg = $conn->consulta($sql);
			
			return $reg;
		}
		public function retiraCarta($id, $carta)
		{
			$conn = new conexao();
			$sql = "DELETE FROM usuarios_cartas
					WHERE cd_carta = '$carta' AND cd_usuario = '$id' ";
			$conn->consulta($sql);		
		}
		public function pesquisaCartas($id){
			$conn = new conexao();
			$sql = "SELECT u.codigo as cd_user, u.nome as user, c.nome, c.codigo, c.img, uc.qtd 
					FROM usuarios u 
					JOIN usuarios_cartas uc 
					ON (u.codigo = uc.cd_usuario)
					JOIN cartas c
					ON(uc.cd_carta = c.codigo)
					WHERE u.codigo <> $id";
			$reg = $conn->consulta($sql);
			
			return $reg;		
		}
		public function propostaCarta($carta, $usuario){
			$conn = new conexao();
			$sql = "SELECT DISTINCT c.nome, c.img, c.codigo, c.descricao, u.nome as dono, u.codigo as cod_dono
					FROM cartas c
					JOIN usuarios_cartas uc 
					ON (uc.cd_carta = c.codigo)
					JOIN usuarios u 
					ON (uc.cd_usuario = u.codigo)
					WHERE c.codigo = '$carta' AND u.codigo = '$usuario'";
			$reg = $conn->consulta($sql);
			
			return $reg;		
		}
		public function cadastraProposta($id, $proposto){
			$conn = new conexao();
			$sql = "INSERT INTO propostas (status, cd_propondo, cd_proposto) VALUES (1, '$id', '$proposto')";
			$conn->consulta($sql);
		}
		public function ultimaProposta(){
			$conn = new conexao();
			$sql = "SELECT MAX(codigo) as codigo FROM propostas";
			$reg = $conn->consulta($sql);
			
			return $conn->fetch_object();
		}
		public function cartaDesejada($id, $status){
			//carta que o usuário quer
			$conn = new conexao();
			$sql = "SELECT c.nome as carta, c.codigo as carddesejo, p.cd_propondo
					FROM propostas p
					JOIN proposta_usuarios pu
					ON p.codigo = pu.cd_proposta
					JOIN usuarios u
					ON u.codigo = pu.cd_usuario
					JOIN cartas c
					ON c.codigo = pu.cd_carta
					WHERE u.codigo = '$id' AND
						  p.status = '$status' AND
						  p.cd_proposto = '$id'";
			$reg = $conn->consulta($sql);
			
			return $reg;		
		}
		public function cartaOfertada($id, $status){
			//Cartas oferecidas pelo proponente 
			$conn = new conexao();
			$sql = "SELECT p.codigo as proposta, c.nome as carta2, c.codigo as cardoferta, u.nome as nome, pu.cd_usuario
					FROM propostas p
					JOIN proposta_usuarios pu
					ON p.codigo = pu.cd_proposta
					JOIN usuarios u
					ON u.codigo = pu.cd_usuario
					JOIN cartas c
					ON c.codigo = pu.cd_carta
					WHERE u.codigo <> '$id' AND
						  p.status = '$status' AND
						  p.cd_proposto = '$id'
						  order by p.codigo asc";
			$reg = $conn->consulta($sql);
			
			return $reg;		
		}		
		public function propostaFeita($id){
			//PROPOSTAS FEITA PELO USUARIO
			$conn = new conexao();
			$sql = "SELECT c.nome as carta, u.nome
					FROM cartas c
					JOIN proposta_usuarios pu
					ON(c.codigo = pu.cd_carta)
					JOIN usuarios u
					ON (pu.cd_usuario = u.codigo)
					JOIN propostas p
					ON(pu.cd_proposta = p.codigo)
					WHERE p.cd_propondo = '$id' AND
						  p.status = 1 AND
						  u.codigo <> '$id'";
		  
			$reg = $conn->consulta($sql);
			
			return $reg;		
		}
		public function propostaOferecida($id){
			//CARTA OFERECIDA PELO USUARIO
			$conn = new conexao();
			$sql = "SELECT c.nome as carta, p.codigo as proposta, u.nome
					FROM cartas c
					JOIN proposta_usuarios pu
					ON(c.codigo = pu.cd_carta)
					JOIN usuarios u
					ON (pu.cd_usuario = u.codigo)
					JOIN propostas p
					ON(pu.cd_proposta = p.codigo)
					WHERE p.cd_propondo = '$id' AND
						  p.status = 1 AND
						  u.codigo = '$id'";
			$reg = $conn->consulta($sql);
		
			return $reg;		
		}
		public function cadastraOferta($card, $owner, $id){
			$conn = new conexao();
			$sql = "INSERT INTO proposta_usuarios (cd_usuario, cd_carta, cd_proposta, qtd)VALUES ('$owner','$card', '$id', '1')";
			$conn->consulta($sql);
		}
		public function mudaStatus($status, $proposta)
		{
			$conn = new conexao();
			$sql = "UPDATE propostas 
					SET status = '$status' 
					WHERE codigo = '$proposta'";
			$conn->consulta($sql);		
		}
		public function verificaTroca($id)
		{
			$conn = new conexao();
			$sql = "SELECT p.codigo
					FROM propostas p
					JOIN proposta_usuarios pu
					ON p.codigo = pu.cd_proposta
					JOIN usuarios u
					ON pu.cd_usuario = u.codigo
					WHERE p.status = 2 AND p.cd_propondo <> '$id'
					AND u.codigo = '$id' ";
			$conn->consulta($sql);
			$linhas = $conn->linhas();
			return $linhas;
		}
		public function selectCarta($dono, $carta)
		{
			$conn = new conexao();
			$sql  = "SELECT cd_usuario, cd_carta, qtd
					 FROM usuarios_cartas
					 WHERE cd_usuario = '$dono' and cd_carta = '$carta'";
			$reg = $conn->consulta($sql);		 
			return $reg;
		}
		public function deletaCarta($dono, $carta)
		{
			$conn = new conexao();
			$sql = "DELETE FROM usuarios_cartas 
					WHERE cd_usuario = '$dono' AND cd_carta = '$carta'";
			$conn->consulta($sql);		
		}

		public function insertCarta($dono, $carta, $qtd)
		{
			$conn = new conexao();
			$sql = "INSERT INTO usuarios_cartas(cd_usuario,cd_carta,qtd) VALUES('$dono','$carta', '$qtd')";
			$conn->consulta($sql);
		}

		public function verificaQuantidade($dono, $carta)
		{
			$conn = new conexao();
			$sql = "SELECT uc.qtd
					FROM usuarios_cartas uc
					WHERE cd_usuario = '$dono' AND cd_carta = '$carta'";
			$conn->consulta($sql);
			$reg = $conn->fetch_object();
			return 	$reg;	
		}
		public function atualizaQuantidade($dono, $carta, $qtd)
		{
			$conn = new conexao();
			$sql = "UPDATE usuarios_cartas 
					SET qtd = '$qtd'
					WHERE cd_usuario = '$dono' and cd_carta = '$carta'";
			$conn->consulta($sql);		 
		}
	}