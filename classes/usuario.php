<?php
require_once('conecta.php');

class usuario{
	private $nome;
	private $senha;
	private $email;
	private $login;
	private $sexo;
	private $rua;
	private $numero;
	private $bairro;
	private $telefone;
	private $uf;
	private $cidade;
	
	function setNome($username){
		$this->nome = $username;
	}
	
	function setSenha($userpass){
		$this->senha = $userpass;
	}

	function setEmail($usermail){
		$this->email = $usermail;
	}

	function setLogin($userlogin){
		$this->login = $userlogin;
	}	
	
	function setSexo($usersex){
		$this->sexo = $usersex;
	}
	
	function getNome(){
		return $this->nome;
	}
	function getLogin(){
		return $this->login;
	}
	function getSenha(){
		return $this->senha;
	}
	function getEmail(){
		return $this->email;
	}
	function getSexo(){
		return $this->sexo;
	}
    
    // FUNCTIONS ENDEREÇO
	function setRua($userrua){
		$this->rua = $userrua;
	}
	function setNumero($usernumero){
		$this->numero = $usernumero;
	}
	function setBairro($userbairro){
		$this->bairro = $userbairro;
	}
	function setCidade($usercidade){
		$this->cidade = $usercidade;
	}
	function setUf($useruf){
		$this->uf = $useruf;
	}
	function setTelefone($userfone){
		$this->telefone = $userfone;
	}
    function getRua(){
		return $this->rua;
	}
	function getBairro(){
		return $this->bairro;
	}
	function getTelefone(){
		return $this->telefone;
	}
	function getNumero(){
		return $this->numero;
	}
	function getUf(){
		return $this->uf;
	}
	function getCidade(){
		return $this->cidade;
	}

}