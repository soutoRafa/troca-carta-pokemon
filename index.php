<!DOCTYPE html>
<html>
<head>
 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PokéTrade</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="fonte/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/home.css">

</head>
<body>
	<section id="cont">
		<div id="logo"></div>
		<span id="logo-label">PokéTrade</span>
		<div style="clear: both;"></div>
		<div class="row">
			<div class="container">
				
				<div class="col-md-8">
					<h1>Comece a trocar agora!</h1>
					<p>PokéTrade é um sistema criado para a interação de colecionadores de cartas Pokémon TCG (trading card game) para que possam achar as cartas que procuram mais facilmente e adquiri-las de outros colecionadores.</p>
					<p><a href="cadastro.php">Cadastre-se</a> no site, ache seus cards e comece a trocar!</p>
				</div>
				<div class="col-md-4">
					<div class="box-login">
					<form method="post" action="validaLogin.php">
						
						<div class="page-header">
							<h3>Acesse sua conta PokéTrade</h3>
						</div>

						<label for="nome">Login</label><br />
                	    <input type="text" name="login" id="nome" class="input"><br />
                    	
                    	<label for="pass">Senha</label><br />
                    	<input type="password" name="pass" id="pass" class="input">  <br />       
                    	
                    	<button type="submit" name="btnlog" id="btncad">Entrar</button>
                    </form>
                    <h5>Não possui uma conta? Cadastre-se <a href="cadastro.php">aqui!</a></h5>
					</div>
				</div>
			</div>	
		</div>	
	</section>
			
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	 <script src="bootstrap/js/bootstrap.min.js"></script>	
	 <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	 <script type="text/javascript" src="js/script.js"></script>
    
</body>
</html>