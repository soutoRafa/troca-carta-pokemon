<?php	
	$nome = $_POST['nome'];
	$login = $_POST['login'];
	$senha = $_POST['pass'];
	$email = $_POST['email'];
	$sexo = $_POST['sexo'];

	require_once 'classes/usuario.php';
	require_once 'classes/conecta.php';
	require_once 'classes/crud.php';

	$novousuario = new usuario();

	$novousuario->setNome($nome);
	$novousuario->setLogin($login);
	$novousuario->setSenha($senha);
	$novousuario->setEmail($email);
	$novousuario->setSexo($sexo);

	$cadastrar = new crud();
	$cadastrar->insert($novousuario);

?>

<script type="text/javascript">
	alert('cadastro realizado com sucesso!');
	window.location.href = 'login.php';	

</script>