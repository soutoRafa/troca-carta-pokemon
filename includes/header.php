<!DOCTYPE html>
<html lang="pt-br">
  <head>
     <meta charset="utf-8">
   	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
  	 <meta name="viewport" content="width=device-width, initial-scale=1">
  	 <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
  	 <link rel="stylesheet" type="text/css" href="../css/user.css">	
     <link rel="stylesheet" href="../fonte/font-awesome/css/font-awesome.min.css">
     <link rel="stylesheet" type="text/css" href="../css/geral.css">
     <link rel="stylesheet" type="text/css" href="../css/perfil.css">
     <link rel="stylesheet" type="text/css" href="../css/cartas.css">
     <link rel="stylesheet" type="text/css" href="../css/trocas.css">
     <link rel="stylesheet" type="text/css" href="../css/propostas.css">

     <title>Usuário</title>
  </head>
 	<body>
   
 		<header id="topo">
 			<div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div id="logo"></div>
                    </div>
 				
                  <div class="col-md-8">        
                    <nav id="menu">
 					  <ul>
                        <li><a href="user.php">Home</a></li>
 						<!-- <li><a href=""> Perfil </a></li>-->
 						 <li><a href="minhasTrocas.php"> Trocas	</a></li>
 						 <li><a href="minhasCartas.php"> Minhas Cartas	</a></li>
 						 <li><a href="logout.php"> Sair </a></li>
 					  </ul>
 			        </nav>
 			      </div><!--/COLUNA MENU-->
                </div>
            </div><!-- container-->
 		</header> 