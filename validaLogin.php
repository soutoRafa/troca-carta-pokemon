<?php
	session_start();

	$login = $_POST['login'];
	$senha = $_POST['pass'];

	require_once 'classes/classLogin.php';

	$log = new login();
	$linhas = $log->logar($login, $senha);

	if ($linhas == 1) {
		$_SESSION['logado'] = true;
		$_SESSION['login']  = $login;
		header('location: site/user.php');
	}else{
		unset($login);
		unset($senha);
	}
?>
<script type="text/javascript">
	alert('login ou senha incorretos!');
	window.location.href = 'login.php';	
</script>